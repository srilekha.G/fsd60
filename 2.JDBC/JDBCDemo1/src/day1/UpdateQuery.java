package day1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;
// updation query

public class UpdateQuery {

	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();

		Statement statement = null;

		Scanner scanner = new Scanner(System.in);

		System.out.println("enter  emplyee id and new salary of employee:");

		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();

		String updateQuery =  "update employee set salary = " + salary + " where empId = " + empId;



		try {
			statement = connection.createStatement();

			int result = statement.executeUpdate(updateQuery);

			if (result > 0) {
				System.out.println(result + " Record(s) Updated...");
			} else {
				System.out.println("Record Updation Failed...");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
