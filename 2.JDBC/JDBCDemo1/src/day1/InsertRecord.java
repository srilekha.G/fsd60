package day1;
import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Statement;
//insertion query
import java.util.Scanner;

import com.db.DbConnection;

public class InsertRecord {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter empId ,empName,salary,gender,doj,emailId,password:");
		int empId = scanner.nextInt();
		String empName = scanner.next();
		double salary = scanner.nextDouble();
		String gender = scanner.next();
		String doj = scanner.next();
		String emailId = scanner.next();
		String password = scanner.next();
		
		String insertQuery = "insert into employee values (" + 
				empId + ", '" + empName + "', " + salary + ", '" + 
				gender + "', '" + doj + "','" + emailId + "', '" + password + "')";
		
				
		try {
			statement = connection.createStatement();
			
			int result = statement.executeUpdate(insertQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

	}

}
