package day1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCday1 {

	public static void main(String[] args) {
		Connection con = null;
		String url="jdbc:mysql://localhost:3306/fsd60";
		
		try {
			//1.Loading the driver
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			//2.Establishing the connection
			
			 con=DriverManager.getConnection(url,"root","root");
			 
			 if(con != null){
				 System.out.println("Successfully established the connection");
				 con.close();
			 }
			 else{
				 System.out.println("Failed to establish the connection");
			 }
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
