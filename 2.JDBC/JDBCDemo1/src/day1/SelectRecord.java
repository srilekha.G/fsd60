package day1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class SelectRecord {

	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		
		Statement statement = null;
		ResultSet resultSet = null;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("enter id of employee to be displayed:");
		
		int empId = scanner.nextInt();
		
		

		String selectQuery = "select * from employee where empId = "+empId;
		
		try {
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(selectQuery);

			if (resultSet != null) {

				while(resultSet.next()) {
					
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

	}

}
