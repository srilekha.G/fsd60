package day1;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Select {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<Object> data = new ArrayList<Object>();
		String selectQuery = "select * from employee";
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			

			if (resultSet != null) {

				while(resultSet.next()) {

					data.add(resultSet.getInt(1));
					

				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}
			 Scanner scanner = new Scanner(System.in);
			 System.out.println("enter empId:");
			 
			 
	            int empid = scanner.nextInt();

	            if (data.contains(empid)) {
	                try (Connection connection2 = DbConnection.getConnection();
	                     Statement statement2 = connection2.createStatement()) {

	                    String selectQuery2 = "SELECT * FROM employee WHERE empid = " + empid;
	                    ResultSet resultSet2 = statement2.executeQuery(selectQuery2);
	                    while (resultSet2.next()) {
	                       
	                        System.out.println("empID: " + resultSet2.getInt(1));
	                        System.out.println("empName: " + resultSet2.getString(2));
	                        System.out.println("empsalary: " + resultSet2.getDouble(3));
	                        System.out.println("gender: " + resultSet2.getString(4));
	                        System.out.println("email: " + resultSet2.getString(5));
	                        System.out.println("password: " + resultSet2.getString(6));
	                        
	                    }
	                }
	            } else {
	                System.out.println("ID not found.");
	            }
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				
				statement.close();
			}
		} catch (SQLException e) {
			System.out.println("No Record(s) Found!!!");
			
			e.printStackTrace();
		}

	}

}
