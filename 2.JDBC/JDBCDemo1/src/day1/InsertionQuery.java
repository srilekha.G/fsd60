package day1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
//insertion query

import com.db.DbConnection;

public class InsertionQuery {

	public static void main(String[] args) {
	Connection connection = DbConnection.getConnection();
	
	Statement statement = null;
	
	
	int empId = 107;
	String empName = "srilekha";
	double salary = 9896.97;
	String gender = "Female";
	String doj = "2024-03-26";
	String emailId = "lekha@gmail.com";
	String password = "567";
	
	
	String insertQuery = "insert into employee values (" + 
			empId + ", '" + empName + "', " + salary + ", '" + 
			gender + "', '" + doj + "','" + emailId + "', '" + password + "')";
			
	try {
		statement = connection.createStatement();
		
		int result = statement.executeUpdate(insertQuery);
		
		if (result > 0) {
			System.out.println(result + " Record(s) Inserted...");
		} else {
			System.out.println("Record Insertion Failed...");
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	try {
		if (connection != null) {
			statement.close();
			connection.close();
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	}

}
