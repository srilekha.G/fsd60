package day02;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class UpdateRun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Employee empId and salary:");
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		System.out.println();
		String updateQuery = "Update employee set salary = ? where empId = ?";
		try {
			preparedStatement = connection.prepareStatement(updateQuery);
						
			preparedStatement.setInt(2, empId);
			
			preparedStatement.setDouble(1, salary);
			
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("salary updated ");
			} else {
				System.out.println("updation Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		

	}

}
