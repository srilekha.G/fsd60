package day02;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class SelectRun {

	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		
		PreparedStatement preparedStatement = null;
		
		ResultSet resultSet = null;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter Employee empId:");
		
		int empId = scanner.nextInt();
		
		String selectQuery = "select  * from employee where empId = ?";
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
						
			preparedStatement.setInt(1, empId);
			
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet != null) {

				while(resultSet.next()) {


					
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}

				} else {
				System.out.println("No Record(s) Found!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		



	}

}
